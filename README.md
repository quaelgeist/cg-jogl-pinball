WICHTIG:

Kommentierung und Varriablennamen deklaration bitte einheitlich in ENGLISCH. Klammern ({}) bitte ohne Absatz anfügen

```
#!java

: public static void main (String[] args){
  ...
}
```


statt

```
#!java

: public static void main (String[] args)
{
  ...
}
```