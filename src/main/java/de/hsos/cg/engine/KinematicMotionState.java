package de.hsos.cg.engine;

import com.bulletphysics.linearmath.MotionState;
import com.bulletphysics.linearmath.Transform;

/**
 *
 * @author sean
 */
    public class KinematicMotionState extends MotionState {

    private Transform worldTransform;

    public KinematicMotionState() {
        worldTransform = new Transform();
        worldTransform.setIdentity();
    }

    @Override
    public Transform getWorldTransform(Transform worldTrans) {
        worldTrans.set(worldTransform);
        return worldTrans;
    }

    @Override
    public void setWorldTransform(Transform worldTrans) {
        worldTransform.set(worldTrans);
    }
}
