/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.cg.engine;

import com.bulletphysics.collision.broadphase.AxisSweep3;
import com.bulletphysics.collision.broadphase.BroadphaseInterface;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;
import com.bulletphysics.linearmath.DebugDrawModes;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.Transform;
import com.jogamp.opengl.util.texture.TextureIO;
import de.hsos.cg.objects.Arrow;
import de.hsos.cg.objects.Border;
import de.hsos.cg.objects.Initiator;
import de.hsos.cg.objects.InvisableWall;
import de.hsos.cg.objects.Menue;
import de.hsos.cg.objects.Pinball;
import de.hsos.cg.objects.RunwayFloor;
import de.hsos.cg.objects.RunwayRamp;
import de.hsos.cg.objects.RunwayWall;
import de.hsos.cg.objects.abstracts.BasicFlipperObject;
import de.hsos.cg.objects.abstracts.BasicGraphicalObject;
import de.hsos.cg.objects.flippers.LeftFlipper;
import de.hsos.cg.objects.flippers.RightFlipper;
import de.hsos.cg.objects.obstacles.Bumper;
import de.hsos.cg.objects.obstacles.LeftBevel;
import de.hsos.cg.objects.obstacles.LeftBlockade;
import de.hsos.cg.objects.obstacles.RightBevel;
import de.hsos.cg.objects.obstacles.RightBlockade;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.io.File;
import java.util.HashSet;
import java.util.Set;
import static javax.media.opengl.GL.GL_COLOR_BUFFER_BIT;
import static javax.media.opengl.GL.GL_DEPTH_BUFFER_BIT;
import javax.media.opengl.GL2;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

/**
 *
 * @author sean / jochen
 */
public final class Bullet {
    private boolean actionLeft = false;
    private boolean actionRight = false;
    private boolean resetBall = true; //Start/Reset Logic
    private boolean menueTrigger = true; //To show Menue
    private boolean menueWahl = true; //To select Menue item
    private final boolean verfolgung = false; //If true it follows the ball
    
    private Set<BasicGraphicalObject> objects = new HashSet<>();

    private Transform worldTransform;

    private DefaultCollisionConfiguration collisionConfiguration;
    private CollisionDispatcher dispatcher;
    private SequentialImpulseConstraintSolver solver;
    private DiscreteDynamicsWorld dynamicsWorld;

    private DefaultMotionState groundMotionState;
    private RigidBodyConstructionInfo groundRigidBodyCI;
    
    private Vector3f fallInertia;

    private CollisionShape groundShape;
    private CollisionShape plateShape;
    
    private RigidBodyConstructionInfo flipperRightBodyConsInfo;
    private RigidBody groundRigidBody;
    private RigidBody plateRigidBody;

    private BroadphaseInterface overlappingPairCache;
    
    //Game Items
    private Pinball pinball;
    private LeftFlipper flipperLeft;
    private RightFlipper flipperRight;
    private Bumper bumper1;
    private Bumper bumper2;
    private Bumper bumper3;
    private Border border;
    private LeftBevel lBevel;
    private RightBevel rBevel;
    private LeftBlockade lBlockade;
    private RightBlockade rBlockade;
    private RunwayWall runWall;
    private RunwayFloor runFloor;
    private RunwayRamp runRamp;
    private Initiator initiator;
    
    //Menue Items
    private Menue menue;
    private Arrow arrow;
    
    public Bullet(GL2 gl) {
        worldTransform = new Transform();
        worldTransform.setIdentity();
        initPhysics(gl);
    }

    public void initPhysics(GL2 gl) {

        //The object that will roughly find out whether bodies are colliding.
        collisionConfiguration = new DefaultCollisionConfiguration();

        Vector3f worldMin = new Vector3f(-1000f, -1000f, -1000f);
        Vector3f worldMax = new Vector3f(1000f, 1000f, 1000f);
        AxisSweep3 sweepBP = new AxisSweep3(worldMin, worldMax);
        overlappingPairCache = sweepBP;

        //The object that will accurately find out whether, when, how, and where bodies are colliding.
        dispatcher = new CollisionDispatcher(collisionConfiguration);

        //The object that will determine what to do after collision.
        solver = new SequentialImpulseConstraintSolver();

        //Initialise the JBullet world.
        dynamicsWorld = new DiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);

        //Set the gravity to 10 metres per second squared (m/s^2). Gravity affects all bodies with a mass larger thanC
        dynamicsWorld.setGravity(new Vector3f(0, -10, -3));
        
        //world debug
        DebugDrawer debug = new DebugDrawer(gl);
        debug.setDebugMode(DebugDrawModes.DRAW_AABB);
        dynamicsWorld.setDebugDrawer(debug);

        //initializeBall
        pinball = new Pinball(gl);
        //add object to the set of rigid elements
        objects.add(pinball);
        //and register as collideable obcet
        dynamicsWorld.addRigidBody(pinball.getRig());
        
        //initializeFlippers
//        flipperLeft = new LeftFlipper(gl, -20f); // make him out of the way
        flipperLeft = new LeftFlipper(gl, -18f);
        objects.add(flipperLeft);
        dynamicsWorld.addRigidBody(flipperLeft.getRig());
        flipperLeft.getRig().setGravity(new Vector3f(0,0,0));
        
        flipperRight = new RightFlipper(gl, 10f);
        objects.add(flipperRight);
        dynamicsWorld.addRigidBody(flipperRight.getRig());
        flipperRight.getRig().setGravity(new Vector3f(0,0,0));
        
        //add bumpers
        bumper1 = new Bumper(gl, -30F, 30F, 0);
        objects.add(bumper1);
        dynamicsWorld.addRigidBody(bumper1.getRig());
        
        bumper2 = new Bumper(gl, 20F, 30F, 0);
        objects.add(bumper2);
        dynamicsWorld.addRigidBody(bumper2.getRig());
        
        bumper3 = new Bumper(gl, -5F, 60F, 0);
        objects.add(bumper3);
        dynamicsWorld.addRigidBody(bumper3.getRig());
        
        //add boarder
        border = new Border(gl, 0,0,-35);
        objects.add(border);
        dynamicsWorld.addRigidBody(border.getRig());

        //add LeftBevel
        lBevel = new LeftBevel(gl, -40F, -83F, -1F);
        objects.add(lBevel);
        dynamicsWorld.addRigidBody(lBevel.getRig());
        
        //add RightBevel
        rBevel = new RightBevel(gl, 30F, -83F, -1F);

        objects.add(rBevel);
        dynamicsWorld.addRigidBody(rBevel.getRig());
        
        //add LeftBlockade
        lBlockade = new LeftBlockade(gl, -35F, -47F, -1F);
        objects.add(lBlockade);
        dynamicsWorld.addRigidBody(lBlockade.getRig());
        
        //add RightBlockade
        rBlockade = new RightBlockade(gl, 25F, -47F, -1F);
        objects.add(rBlockade);
        dynamicsWorld.addRigidBody(rBlockade.getRig());
        
        //add RunwayWall
        runWall = new RunwayWall(gl, 49F, -28F, -5F);
        objects.add(runWall);
        dynamicsWorld.addRigidBody(runWall.getRig());
        
        //add RunwayFloor
        runFloor = new RunwayFloor(gl, 54F, -28F, -5F);
        objects.add(runFloor);
        dynamicsWorld.addRigidBody(runFloor.getRig());
        
        //add RunwayRamp
        runRamp = new RunwayRamp(gl, 44.5F, 65F, -5F);
        objects.add(runRamp);
        dynamicsWorld.addRigidBody(runRamp.getRig());        
        
        //add Initiator
        initiator = new Initiator(gl, 53, -120, 5);
        objects.add(initiator);
        
        //Only add Menue Bar if "menueTrigger" is true
        if(menueTrigger){
            //add Initiator
            menue = new Menue(gl, 0, -70, 25);
            objects.add(menue);

            //add Initiator
            arrow = new Arrow(gl, 25, 3, 70);
            objects.add(arrow);
        }
        
        //add invisable wals
        makeBorders(gl);
    }

    public void renderWithPhysics(GL2 gl) {
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        for (BasicGraphicalObject body : objects){
            gl.glPushMatrix();
            gl.glColor3f(0.0f, 0.0f, 0.0f);
            Vector3f elementPosition;
            elementPosition = body.getRig().getWorldTransform(new Transform()).origin;
            gl.glTranslatef(elementPosition.x, elementPosition.y, elementPosition.z);
            //Special treatment for Flippers
            if (body.equals(flipperLeft)) {
                if (actionLeft == false) {
                    Quat4f angleq = new Quat4f();
//                    flipperLeft.getRig().getOrientation(angleq);
//                    angleq.
                    gl.glRotatef(-90f, 1f, 0f, 0f);
                    gl.glRotatef(45f, 0f, 1f, 0f);
                } else {
                    gl.glRotatef(-90f, 1f, 0f, 0f);
                    gl.glRotatef(-45f, 0f, 1f, 0f);
                }
            } else if (body.equals(flipperRight)) {
                if (actionRight == false) {
                    gl.glRotatef(90f, 1f, 0f, 0f);
                    gl.glRotatef(45f, 0f, 1f, 0f);
                } else {
                    gl.glRotatef(90f, 1f, 0f, 0f);
                    gl.glRotatef(-45f, 0f, 1f, 0f);
                }
            //Special treatment for Menue and Arrow
            }else if (body.equals(menue)) {
                gl.glRotatef(20f, 1f, 0f, 0f);
                gl.glRotatef(180f, 0f, 0f, 1f);
            }else if (body.equals(arrow)) {
                gl.glRotatef(20f, 1f, 0f, 0f);
            }
            //Texture for Menue
            if(body.equals(menue)){
                try{
                    TextureIO.newTexture(new File("./resources/textures/Menue.jpg") , true).enable(gl);
                    body.drawModel(gl);
                    TextureIO.newTexture(new File("./resources/textures/Menue.jpg") , true).disable(gl);
                }catch(Exception e){
                    body.drawModel(gl);
                    System.out.println("Fehler beim lesen der datei");
                }
            }else{//All Other Objects
                body.drawModel(gl);
            }
            gl.glPopMatrix();
        }
        
        //Debugging Class for Collusion Objects and Invisible Walls
//        dynamicsWorld.debugDrawWorld();
    }
    
    //Create Invisible Walls for siple Objects
    public void makeBorders(GL2 gl){
        InvisableWall leftWall = new InvisableWall(-59, -20, 0, 2, 90, 10);
        InvisableWall rightWall = new InvisableWall(60, -20, 0, 2, 90, 10);
        InvisableWall capWall = new InvisableWall(0, 5, 5 , 80, 120, 0.5F);
        InvisableWall BackWall = new InvisableWall(0, 5, -5, 80, 108, 0.5F);
        InvisableWall ground = new InvisableWall(0, -110, 0, 80, 5, 10);
        InvisableWall topWall1 = new InvisableWall(0, 100, 0, 50, 2, 10);
        InvisableWall topWall2 = new InvisableWall(45, 80, 0, 30, 2, 10, -1, -2, 0, 0);
        InvisableWall topWall3 = new InvisableWall(-45, 80, 0, 30, 2, 10, 1, -2, 0, 0);

        dynamicsWorld.addRigidBody(leftWall.getRig());
        dynamicsWorld.addRigidBody(rightWall.getRig());
        dynamicsWorld.addRigidBody(capWall.getRig());
        dynamicsWorld.addRigidBody(BackWall.getRig());
        dynamicsWorld.addRigidBody(topWall1.getRig());
        dynamicsWorld.addRigidBody(topWall2.getRig());
        dynamicsWorld.addRigidBody(topWall3.getRig());
        dynamicsWorld.addRigidBody(ground.getRig());
    }

    public void logic(GL2 gl) {
        // Reset the model-view matrix.
        gl.glLoadIdentity();
        // Runs the JBullet physics simulation for the specified time in seconds.
        dynamicsWorld.stepSimulation(1f, 6);

        if (actionLeft == true) {
            addHighness(true, flipperLeft);
        } else {
            addHighness(false, flipperLeft);
        }
        if (actionRight == true) {
            addHighness(true, flipperRight);
        } else {
            addHighness(false, flipperRight);
        }
        
        Vector3f ballspeed = new Vector3f();
        pinball.getRig().getLinearVelocity(ballspeed);
        float speed = ballspeed.length();
        //System.out.println("Ballspeed: " + ballspeed.length());
        
        if (speed > pinball.getMaxSpeed()){
            ballspeed.scale(10/pinball.getMaxSpeed());
            pinball.getRig().setLinearVelocity(ballspeed);
            pinball.getRig().setAngularVelocity(ballspeed);
        }
    }
    
    public void addHighness(boolean state, BasicFlipperObject flipper) {
        
//        float maxPoint = -85;
        Vector3f stop = new Vector3f(0,0,0);
//        float position = flipper.getMotion().getWorldTransform(new Transform()).origin.y;

//        flipper.getRig().setCenterOfMassTransform(flipper.getDefaultPosition());
        flipper.getRig().setAngularVelocity(new Vector3f(stop));
        flipper.getRig().setLinearVelocity(new Vector3f(stop));
        
        Quat4f currentRotation = new Quat4f();
        flipper.getRig().getOrientation(currentRotation);
        
        /*print the quaternion status*/
//        NumberFormat n = new DecimalFormat("0.000");
//        n.setRoundingMode(RoundingMode.DOWN);
//        System.out.println(
//                        " x: " + n.format(currentRotation.x) +
//                        " y: " + n.format(currentRotation.y) + 
//                        " z: " + n.format(currentRotation.z) + 
//                        " w: " + n.format(currentRotation.w));
        if (state) {
            
            if (flipper.reachedLimitUp(currentRotation.z)){
                flipper.getRig().applyTorqueImpulse(flipper.getUpDir());
//                if (position < maxPoint){
//                    flipper.getRig().applyCentralImpulse(upForce);
//                }
            }   
        } else {
            if (flipper.reachedLimitDown(currentRotation.z)){
                flipper.getRig().applyTorqueImpulse(flipper.getDownDir());
//                if (position > flipper.getyLocation()){
//                    flipper.getRig().applyCentralImpulse(downForce);
//                }
                
            }   
        }
//        System.out.println("downstate : \t" + flipperLeft.getQuaternialDownState());
//        System.out.println("upstate : \t" + flipperLeft.getQuaternialUpState());
//        System.out.println("current : \t" + n.format(currentRotation.z));
    }
    
    /**Some Menue Actions*/
    
    //Method to push the ball and start the game
    public void pushBall(){
        if(this.resetBall == true){
            pinball.pushBall();
            this.resetBall = false;
            initiator.start();
        }
    }
    
    //Method to reset the ball
    public void setResetBall() {
        if(this.resetBall == false){
            pinball.resetBall();
            this.resetBall = true;
            this.initiator.reset();
        }
    }
    
    //Method to follow the ball, if "verfolgung" is true
    public float getPinballPositionX(){
        if(menueTrigger){
            return 0F;
        }else if (verfolgung){
            return pinball.getMotion().getWorldTransform(new Transform()).origin.x*0.05F;
        }else{
            return 0F;
        }
    }
    
    //Method to follow the ball, if "verfolgung" is true
    public float getPinballPositionY(){
        if(menueTrigger){
            return 0F;
        }else if (verfolgung){
            return pinball.getMotion().getWorldTransform(new Transform()).origin.y*0.05F;
        }else{
            return 0F;
        }
    }
        
    //Method to follow the ball, if "verfolgung" is true
    public float getPinballPositionZ(){
        if(menueTrigger){
            return 0F;
        }else if (verfolgung){
            return pinball.getMotion().getWorldTransform(new Transform()).origin.z*0.05F;
        }else{
            return 0F;
        }
    }
    
    //To get the Menue status
    public boolean getMenueStatus(){
        return this.menueTrigger;
    }
    
    //To switch between the two Menue-Items
    public void arrowNext(){
        this.arrow.next();
        menueWahl = !menueWahl;
    }
    
    //To enter the Menue
    public void hitMenue(){
        if(menueWahl){
            objects.remove(arrow);
            objects.remove(menue);
            menueTrigger = false;
        } else{
            System.exit(-1);
        }
    }
    
    /** Just some getters and setters */
    
    public DefaultCollisionConfiguration getCollisionConfiguration() {
        return collisionConfiguration;
    }

    public void setCollisionConfiguration(DefaultCollisionConfiguration collisionConfiguration) {
        this.collisionConfiguration = collisionConfiguration;
    }

    public CollisionDispatcher getDispatcher() {
        return dispatcher;
    }

    public void setDispatcher(CollisionDispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    public SequentialImpulseConstraintSolver getSolver() {
        return solver;
    }

    public void setSolver(SequentialImpulseConstraintSolver solver) {
        this.solver = solver;
    }

    public DiscreteDynamicsWorld getDynamicsWorld() {
        return dynamicsWorld;
    }

    public void setDynamicsWorld(DiscreteDynamicsWorld dynamicsWorld) {
        this.dynamicsWorld = dynamicsWorld;
    }

    public CollisionShape getGroundShape() {
        return groundShape;
    }

    public void setGroundShape(CollisionShape groundShape) {
        this.groundShape = groundShape;
    }

    public DefaultMotionState getGroundMotionState() {
        return groundMotionState;
    }

    public void setGroundMotionState(DefaultMotionState groundMotionState) {
        this.groundMotionState = groundMotionState;
    }

    public RigidBodyConstructionInfo getGroundRigidBodyCI() {
        return groundRigidBodyCI;
    }

    public void setGroundRigidBodyCI(RigidBodyConstructionInfo groundRigidBodyCI) {
        this.groundRigidBodyCI = groundRigidBodyCI;
    }

    public RigidBody getGroundRigidBody() {
        return groundRigidBody;
    }

    public void setGroundRigidBody(RigidBody groundRigidBody) {
        this.groundRigidBody = groundRigidBody;
    }

    public Vector3f getFallInertia() {
        return fallInertia;
    }

    public void setFallInertia(Vector3f fallInertia) {
        this.fallInertia = fallInertia;
    }
    
    public boolean isActionLeft() {
        return actionLeft;
    }

    public void setActionLeft(boolean actionLeft) {
        this.actionLeft = actionLeft;
    }

    public boolean isActionRight() {
        return actionRight;
    }

    public void setActionRight(boolean actionRight) {
        this.actionRight = actionRight;
    }

    public CollisionShape getPlateShape() {
        return plateShape;
    }

    public void setPlateShape(CollisionShape plateShape) {
        this.plateShape = plateShape;
    }

    public RigidBodyConstructionInfo getFlipperRightBodyConsInfo() {
        return flipperRightBodyConsInfo;
    }

    public void setFlipperRightBodyConsInfo(RigidBodyConstructionInfo flipperRightBodyConsInfo) {
        this.flipperRightBodyConsInfo = flipperRightBodyConsInfo;
    }

    public RigidBody getPlateRigidBody() {
        return plateRigidBody;
    }

    public void setPlateRigidBody(RigidBody plateRigidBody) {
        this.plateRigidBody = plateRigidBody;
    }

    public Pinball getPinball() {
        return pinball;
    }

    public void setPinball(Pinball pinball) {
        this.pinball = pinball;
    }

    public LeftFlipper getFlipperLeft() {
        return flipperLeft;
    }

    public void setFlipperLeft(LeftFlipper flipperLeft) {
        this.flipperLeft = flipperLeft;
    }
    
    
}
