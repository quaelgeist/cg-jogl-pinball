
package de.hsos.cg.engine;

import de.hsos.cg.prototypes.BasicReflectionAndCollision;
import javax.swing.JFrame;

/**
 *
 * @author sean
 */
public class WindowManager{
    
    private static WindowManager instance = new WindowManager();
    
    int width = 900;
    int height = 1200;
    
    private WindowManager(){
        JFrame window = new JFrame();
        window.getContentPane().add(new BasicReflectionAndCollision());
        window.setSize(width, height);
        window.setVisible(true);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    public static WindowManager getInstance(){
        return WindowManager.instance;
    }
    
    
}
