/*
    For testing
*/

///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package de.hsos.cg.prototypes;
//
//import com.bulletphysics.linearmath.Transform;
//import com.jogamp.opengl.util.FPSAnimator;
//import de.hsos.cg.engine.Bullet;
//import de.hsos.cg.engine.WindowManager;
//import de.hsos.cg.objects.Pinball;
//import java.awt.event.KeyEvent;
//import java.awt.event.KeyListener;
//import javax.media.opengl.GL2;
//import javax.media.opengl.GLAutoDrawable;
//import javax.media.opengl.GLEventListener;
//import javax.media.opengl.awt.GLJPanel;
//import javax.media.opengl.glu.GLU;
//import javax.swing.JFrame;
//
//@SuppressWarnings("serial")
//public final class MovementControll extends GLJPanel implements GLEventListener, KeyListener {
//
//    public static int width = 1000;
//    public static int height = 1000;
//
//    public Pinball pb = null;
//
//    public Bullet bullet = null;
//    
//    private float vertical = 45;
//    private float horizontal = 45;
//
//    public MovementControll() {
//        setFocusable(true);
//        addGLEventListener(this);
//        addKeyListener(this);
//        FPSAnimator animator = new FPSAnimator(this, 60);
//        animator.start();
//    }
//
//    public static void main(String[] args) {
//        WindowManager.getInstance();
//
//    }
//
//    @Override
//    public void display(GLAutoDrawable drawable) {
//        GL2 gl = drawable.getGL().getGL2();
//        
//        bullet.renderWithPhysics(gl);
//        bullet.logic(gl);
////        update(gl);
////        render(gl);
//        reInit(gl);
//    }
//    
//    public void reInit(GL2 gl){
//        gl.glRotatef(horizontal, 1f, 0f, 0f);
//        gl.glRotatef(vertical, 0f, 1f, 0f);
//    }
//
//    public void update(GL2 gl){
//        gl.glLoadIdentity();
//        bullet.getDynamicsWorld().stepSimulation(1f, 6);
//        Transform trans = new Transform();
//        bullet.getBallRigidBody().getMotionState().getWorldTransform(trans);
//        pb.setyLocation(trans.origin.y);
//        pb.setxLocation(trans.origin.x);
//        pb.setxLocation(trans.origin.z);
//    }
//    
//    public void render(GL2 gl) {
//        gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
//        gl.glLoadIdentity();
//        gl.glPushMatrix();
//
//        gl.glTranslatef(pb.getxLocation(), pb.getyLocation(), -1.0f);
//        pb.drawPinnball(gl);
//
//        gl.glPopMatrix();
//        gl.glFlush();
//    }
//    
//    public void setLight(GL2 gl) {
//
//        gl.glEnable(GL2.GL_LIGHTING);
//        float SHINE_ALL_DIRECTIONS = 1;
//        float[] lightPos = {0, 10, 10, SHINE_ALL_DIRECTIONS};
//        float[] lightColorAmbient = {0f, 0f, 1f, 1f};
//        float[] lightColorDiffuse = {1f, 0f, 0f, 1f};
//        float[] lightColorSpecular = {1f, 1f, 1f, 1f};
//
//        // Set light parameters.
//        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_POSITION, lightPos, 0);
//        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_AMBIENT, lightColorAmbient, 0);
//        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_SPECULAR, lightColorSpecular, 0);
//        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_DIFFUSE, lightColorDiffuse, 0);
//        gl.glEnable(GL2.GL_LIGHT1);
//
//    }
//
//    @Override
//    public void init(GLAutoDrawable drawable) {
//        GL2 gl = drawable.getGL().getGL2();
//        
//        bullet = new Bullet(gl);
//        
//        gl.glClearColor(0.5f, 0.5f, 0.5f, 0.5f); // set background (clear) color
//        gl.glClearDepth(1.0f);      // set clear depth value to farthest
//        gl.glEnable(GL2.GL_DEPTH_TEST); // enables depth testing
//        gl.glDepthFunc(GL2.GL_LEQUAL);  // the type of depth test to do
//        gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST); // best perspective correction
//        gl.glShadeModel(GL2.GL_SMOOTH); // blends colors nicely, and smoothes out lighting
//        gl.glEnable(GL2.GL_CULL_FACE);
//        gl.glEnable(GL2.GL_NORMALIZE);
//
//        gl.glMatrixMode(GL2.GL_PROJECTION);
//        gl.glLoadIdentity();
//        gl.glOrtho(-100f, 100f, -100f, 100f, -200f, 200f);
//        gl.glMatrixMode(GL2.GL_MODELVIEW);
//        gl.glLoadIdentity();
//
////        setLight(gl);
//        pb = new Pinball(gl);
////        pb2 = new Pinball(gl);
//
//    }
//
//    @Override
//    public void keyPressed(KeyEvent e) {
//        int key = e.getKeyCode();  // Tells which key was pressed.
////        System.out.println("Key pressen: " + key);
//        if (key == KeyEvent.VK_A) {
//            pb.setxLocation(pb.getxLocation()-5);
//        } else if (key == KeyEvent.VK_D) {
//            pb.setxLocation(pb.getxLocation()+5);
//        } else if (key == KeyEvent.VK_W) {
//            pb.setyLocation(pb.getyLocation()+5);
//        } else if (key == KeyEvent.VK_S) {
//            pb.setyLocation(pb.getyLocation()-5);
//        } else if (key == KeyEvent.VK_NUMPAD5) {
//            horizontal -= 5;
//        } else if (key == KeyEvent.VK_NUMPAD8) {
//            horizontal += 5;
//        } else if (key == KeyEvent.VK_NUMPAD4) {
//            vertical -= 5;
//        } else if (key == KeyEvent.VK_NUMPAD6) {
//            vertical += 5;
//        } else {
////            System.out.println("typed: " + key);
//        }
//
//        repaint();
//    }
//
//    @Override
//    public void keyTyped(KeyEvent e) {
//        char ch = e.getKeyChar();
//
//    }
//
//    @Override
//    public void keyReleased(KeyEvent e) {
//    }
//
//    @Override
//    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
//
//        GL2 gl = drawable.getGL().getGL2();
//
//        gl.glMatrixMode(GL2.GL_PROJECTION);
//        gl.glLoadIdentity();
//        GLU glu = new GLU();
//        gl.glOrtho(-100f, 100f, -100f, 100f, -200f, 200f);
//        gl.glMatrixMode(GL2.GL_MODELVIEW);
//    }
//
//    @Override
//    public void dispose(GLAutoDrawable drawable) {
//    }
//
//}
