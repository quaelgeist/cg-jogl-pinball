/*
    For testing
*/
package de.hsos.cg.prototypes;

import com.bulletphysics.collision.broadphase.BroadphaseInterface;
import com.bulletphysics.collision.broadphase.DbvtBroadphase;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.SphereShape;
import com.bulletphysics.collision.shapes.StaticPlaneShape;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.MotionState;
import com.bulletphysics.linearmath.Transform;
import de.hsos.cg.objects.flippers.LeftFlipper;
import de.hsos.cg.objects.Pinball;
import java.util.HashSet;
import java.util.Set;
import static javax.media.opengl.GL.GL_COLOR_BUFFER_BIT;
import static javax.media.opengl.GL.GL_DEPTH_BUFFER_BIT;
import javax.media.opengl.GL2;
import static javax.media.opengl.GL2GL3.GL_QUADS;
import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

/**
 *
 * @author sean
 */
public final class BulletPrototypeReflectionAndAngle {

    private Set<RigidBody> rigidElements = new HashSet<RigidBody>();

    private BroadphaseInterface broadphase;
    private DefaultCollisionConfiguration collisionConfiguration;
    private CollisionDispatcher dispatcher;
    private SequentialImpulseConstraintSolver solver;
    private DiscreteDynamicsWorld dynamicsWorld;
    
    private DefaultMotionState groundMotionState;
    private RigidBodyConstructionInfo groundRigidBodyCI;
    
    private MotionState ballMotion;
    private MotionState flipperLeftMotion;
    private Vector3f fallInertia;
    
    private CollisionShape groundShape;
    private CollisionShape ballShape;
    private CollisionShape flipperRightShape;
    private CollisionShape flipperLeftShape;
    private CollisionShape plateShape;

    private RigidBodyConstructionInfo ballRigidBodyConsInfo;
    private RigidBodyConstructionInfo flipperRightBodyConsInfo;
    private RigidBodyConstructionInfo flipperLeftBodyConsInfo;
    
    private RigidBody groundRigidBody;
    private RigidBody ballRigidBody;
    private RigidBody flipperRightRigidBody;
    private RigidBody flipperLeftRigidBody;
    private RigidBody plateRigidBody;
    int mass = 1;
    
    //test objects
    Pinball pinball = null;
    LeftFlipper flipperLeft = null;
    

    public BulletPrototypeReflectionAndAngle(GL2 gl) {
        initPhysics();
        pinball = new Pinball(gl);
        flipperLeft = new LeftFlipper(gl);
    }

    public void initPhysics() {

        //The object that will roughly find out whether bodies are colliding.
        broadphase = new DbvtBroadphase();
        collisionConfiguration = new DefaultCollisionConfiguration();

        //The object that will accurately find out whether, when, how, and where bodies are colliding.
        dispatcher = new CollisionDispatcher(collisionConfiguration);

        //The object that will determine what to do after collision.
        solver = new SequentialImpulseConstraintSolver();

        //Initialise the JBullet world.
        dynamicsWorld = new DiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);

        //Set the gravity to 10 metres per second squared (m/s^2). Gravity affects all bodies with a mass larger thanC
        dynamicsWorld.setGravity(new Vector3f(0, -10, 0));

        //Initialise 'groundShape' to a static plane shape on the origin facing upwards ([0, 1, 0] is the normal).
        //0.25 metres is an added buffer between the ground and potential colliding bodies, used to prevent the bodies
        //from partially going through the floor. It is also possible to think of this as the plane being lifted 0.25m.
        groundShape = new StaticPlaneShape(new Vector3f(0, 1, 0), 0.25f);
        
        //Initialise 'groundMotionState' to a motion state that simply assigns the origin [0, 0, 0] as the origin of
        //the ground.
        groundMotionState = new DefaultMotionState(new Transform(new Matrix4f(new Quat4f(0, 0, 0, 1), new Vector3f(0, 0, 0), 1.0f)));

        //Initialise 'groundBodyConstructionInfo' to a value that contains the mass, the motion state, the shape, and the inertia (= resistance to change).
        groundRigidBodyCI = new RigidBodyConstructionInfo(0, groundMotionState, groundShape, new Vector3f(0, 0, 0));

        // Set the restitution, also known as the bounciness or spring, to 0.25. The restitution may range from 0.0
        // not bouncy) to 1.0 (extremely bouncy).
        groundRigidBodyCI.restitution = 1.f;

        // Initialise 'groundRigidBody', the final variable representing the ground, to a rigid body with the previously
        // assigned construction information.
        groundRigidBody = new RigidBody(groundRigidBodyCI);

        // Add the ground to the JBullet world.
        dynamicsWorld.addRigidBody(groundRigidBody);
        
        
        //ball
        //Initialise 'ballShape' to a sphere with a radius of 3 metres.
        ballShape = new SphereShape(3);
        //Initialise 'Motion' to a motion state that assigns a specified location.
        ballMotion = new DefaultMotionState(new Transform(new Matrix4f(new Quat4f(0, 0, 0, 1), new Vector3f(-1, 35, 0), 1.0f)));
        //Calculate the inertia (resistance to movement) using the mass (2.5 kilograms).
        Vector3f ballInertia = new Vector3f(0, 0, 0);
        ballShape.calculateLocalInertia(mass, ballInertia);
        //Composes the construction info of its mass, its motion state, its shape, and its inertia.
        ballRigidBodyConsInfo = new RigidBodyConstructionInfo(mass, ballMotion, ballShape, ballInertia);
        //Set the restitution, also known as the bounciness or spring, to 0.5. The restitution may range from 0.0
        //not bouncy) to 1.0 (extremely bouncy).
        ballRigidBodyConsInfo.restitution = 0.5f;
        ballRigidBodyConsInfo.angularDamping = 0.95f;
        //now we add it to our physics simulation 
        ballRigidBody = new RigidBody(ballRigidBodyConsInfo);
        //add object to the set of rigid elements
        rigidElements.add(ballRigidBody);
        //and register as collideable obcet
        dynamicsWorld.addRigidBody(ballRigidBody);
        

        //left Flipper. almost the same as the ball
//        flipperLeftShape = new BoxShape(new Vector3f(2, 2,2));
//        flipperLeftShape = new CapsuleShape(1, 7);
//        flipperLeftShape = new SphereShape(3);
//        flipperLeftMotion = new DefaultMotionState(new Transform(new Matrix4f(new Quat4f(1, 1, 1, 2), new Vector3f(0, 4, 0), 1.0f)));
//            flipperLeftShape = new CapsuleShape(3, 20);
        flipperLeftShape = new BoxShape(new Vector3f(7, 2, 5));
        
//        flipperLeftMotion = new DefaultMotionState(new Transform(new Matrix4f(new Quat4f(-1, -2, 0, 0), new Vector3f(0, 0, 0), 1.0f))); save
        
        Transform T = new Transform(new Matrix4f(new Quat4f(-1, -3, 0, 0), new Vector3f(0, 5, 0), 1.0f));
        flipperLeftMotion = new DefaultMotionState(T);
        
        flipperLeftBodyConsInfo = new RigidBodyConstructionInfo(0, flipperLeftMotion, flipperLeftShape);
        flipperLeftBodyConsInfo.restitution= 0.5f;
        
        flipperLeftRigidBody = new RigidBody(flipperLeftBodyConsInfo);
        
        
        //Example for translation
//        Transform T = new Transform(new Matrix4f(new Quat4f(-1, -3, 0, 0), new Vector3f(0, 5, 0), 1.0f));
//        flipperLeftRigidBody.setCenterOfMassTransform(T);
        
        rigidElements.add(ballRigidBody);
        rigidElements.add(flipperLeftRigidBody);
        dynamicsWorld.addRigidBody(flipperLeftRigidBody);
        
    }
    
    public void renderWithPhysics(GL2 gl){
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        for (RigidBody body : rigidElements){

            gl.glPushMatrix();
            gl.glColor3f(0.0f, 0.0f, 0.0f);
            Vector3f elementPosition = body.getWorldTransform(new Transform()).origin;
            gl.glTranslatef(elementPosition.x, elementPosition.y, elementPosition.z);
            
            
            if (body.equals(ballRigidBody)){
                pinball.drawModel(gl);
            } else if (body.equals(flipperLeftRigidBody)){
                gl.glRotatef(-90f, 1f, 0f, 0f);
                gl.glRotatef(20f, 0f, 1f, 0f);
                flipperLeft.drawModel(gl);
            }
            
            
            gl.glPopMatrix();
        }
        
//         Draw the floor.
            gl.glBegin(GL_QUADS);
            
                gl.glColor3f(0.6f, 0.6f, 0.6f);
                gl.glVertex3f(-500f, 0f, -500f);
                gl.glVertex3f(-500f, 0f, +500f);
                gl.glVertex3f(+500f, 0f, +500f);
                gl.glVertex3f(+500f, 0f, -500f);
            
            gl.glEnd();
    }
    
    public void drawBox(GL2 gl){
        gl.glBegin(GL2.GL_POLYGON);/* f1: front */
        gl.glNormal3f(-1.0f,0.0f,0.0f);
        gl.glVertex3f(0.0f,0.0f,0.0f);
        gl.glVertex3f(0.0f,0.0f,1.0f);
        gl.glVertex3f(1.0f,0.0f,1.0f);
        gl.glVertex3f(1.0f,0.0f,0.0f);
      gl.glEnd();
      gl.glBegin(GL2.GL_POLYGON);/* f2: bottom */
        gl.glNormal3f(0.0f,0.0f,-1.0f);
        gl.glVertex3f(0.0f,0.0f,0.0f);
        gl.glVertex3f(4.0f,0.0f,0.0f);
        gl.glVertex3f(4.0f,2.0f,0.0f);
        gl.glVertex3f(0.0f,2.0f,0.0f);
      gl.glEnd();
      gl.glBegin(GL2.GL_POLYGON);/* f3:back */
        gl.glNormal3f(1.0f,0.0f,0.0f);
        gl.glVertex3f(1.0f,1.0f,0.0f);
        gl.glVertex3f(1.0f,1.0f,1.0f);
        gl.glVertex3f(0.0f,1.0f,1.0f);
        gl.glVertex3f(0.0f,1.0f,0.0f);
      gl.glEnd();
      gl.glBegin(GL2.GL_POLYGON);/* f4: top */
        gl.glNormal3f(0.0f,0.0f,1.0f);
        gl.glVertex3f(1.0f,1.0f,1.0f);
        gl.glVertex3f(1.0f,0.0f,1.0f);
        gl.glVertex3f(0.0f,0.0f,1.0f);
        gl.glVertex3f(0.0f,1.0f,1.0f);
      gl.glEnd();
      gl.glBegin(GL2.GL_POLYGON);/* f5: left */
        gl.glNormal3f(0.0f,1.0f,0.0f);
        gl.glVertex3f(0.0f,0.0f,0.0f);
        gl.glVertex3f(0.0f,1.0f,0.0f);
        gl.glVertex3f(0.0f,1.0f,1.0f);
        gl.glVertex3f(0.0f,0.0f,1.0f);
      gl.glEnd();
      gl.glBegin(GL2.GL_POLYGON);/* f6: right */
        gl.glNormal3f(0.0f,-1.0f,0.0f);
        gl.glVertex3f(1.0f,0.0f,0.0f);
        gl.glVertex3f(1.0f,0.0f,1.0f);
        gl.glVertex3f(1.0f,1.0f,1.0f);
        gl.glVertex3f(1.0f,1.0f,0.0f);
      gl.glEnd();
    }
    
    public void logic(GL2 gl) {
        // Reset the model-view matrix.
        gl.glLoadIdentity();
        // Runs the JBullet physics simulation for the specified time in seconds.
        dynamicsWorld.stepSimulation(1f,2);
        // For every element
//        for (RigidBody body : rigidElements) {
//            // Retrieve the ball's position from the JBullet body object.
//            Vector3f position = body.getMotionState().getWorldTransform(new Transform()).origin;
//            
//        }
    }

    public CollisionShape getBallShape() {
        return ballShape;
    }

    public void setBallShape(CollisionShape ballShape) {
        this.ballShape = ballShape;
    }

    public MotionState getBallMotion() {
        return ballMotion;
    }

    public void setBallMotion(MotionState ballMotion) {
        this.ballMotion = ballMotion;
    }
    
    public BroadphaseInterface getBroadphase() {
        return broadphase;
    }

    public void setBroadphase(BroadphaseInterface broadphase) {
        this.broadphase = broadphase;
    }

    public DefaultCollisionConfiguration getCollisionConfiguration() {
        return collisionConfiguration;
    }

    public void setCollisionConfiguration(DefaultCollisionConfiguration collisionConfiguration) {
        this.collisionConfiguration = collisionConfiguration;
    }

    public CollisionDispatcher getDispatcher() {
        return dispatcher;
    }

    public void setDispatcher(CollisionDispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    public SequentialImpulseConstraintSolver getSolver() {
        return solver;
    }

    public void setSolver(SequentialImpulseConstraintSolver solver) {
        this.solver = solver;
    }

    public DiscreteDynamicsWorld getDynamicsWorld() {
        return dynamicsWorld;
    }

    public void setDynamicsWorld(DiscreteDynamicsWorld dynamicsWorld) {
        this.dynamicsWorld = dynamicsWorld;
    }

    public CollisionShape getGroundShape() {
        return groundShape;
    }

    public void setGroundShape(CollisionShape groundShape) {
        this.groundShape = groundShape;
    }

    public CollisionShape getFallShape() {
        return ballShape;
    }

    public void setFallShape(CollisionShape fallShape) {
        this.ballShape = fallShape;
    }

    public DefaultMotionState getGroundMotionState() {
        return groundMotionState;
    }

    public void setGroundMotionState(DefaultMotionState groundMotionState) {
        this.groundMotionState = groundMotionState;
    }

    public RigidBodyConstructionInfo getGroundRigidBodyCI() {
        return groundRigidBodyCI;
    }

    public void setGroundRigidBodyCI(RigidBodyConstructionInfo groundRigidBodyCI) {
        this.groundRigidBodyCI = groundRigidBodyCI;
    }

    public RigidBody getGroundRigidBody() {
        return groundRigidBody;
    }

    public void setGroundRigidBody(RigidBody groundRigidBody) {
        this.groundRigidBody = groundRigidBody;
    }

    public RigidBodyConstructionInfo getBallRigidBodyConsInfo() {
        return ballRigidBodyConsInfo;
    }

    public void setBallRigidBodyConsInfo(RigidBodyConstructionInfo ballRigidBodyConsInfo) {
        this.ballRigidBodyConsInfo = ballRigidBodyConsInfo;
    }

    public Vector3f getFallInertia() {
        return fallInertia;
    }

    public void setFallInertia(Vector3f fallInertia) {
        this.fallInertia = fallInertia;
    }

    public RigidBody getBallRigidBody() {
        return ballRigidBody;
    }

    public void setBallRigidBody(RigidBody ballRigidBody) {
        this.ballRigidBody = ballRigidBody;
    }

    public int getMass() {
        return mass;
    }

    public void setMass(int mass) {
        this.mass = mass;
    }

}
