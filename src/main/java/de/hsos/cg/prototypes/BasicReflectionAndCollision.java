/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.cg.prototypes;

import com.jogamp.opengl.util.FPSAnimator;
import de.hsos.cg.engine.Bullet;
import de.hsos.cg.engine.WindowManager;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.awt.GLJPanel;
import static javax.media.opengl.fixedfunc.GLLightingFunc.GL_COLOR_MATERIAL;
import javax.media.opengl.glu.GLU;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;


@SuppressWarnings("serial")
public final class BasicReflectionAndCollision extends GLJPanel implements GLEventListener, KeyListener {

    private GL2 gl = null;
    private GLU glu = null;
    public Bullet bullet = null;
    private float vertical = 0;
    private float horizontal = 0;
    
    public BasicReflectionAndCollision() {
        setFocusable(true);
        addGLEventListener(this);
        addKeyListener(this);
        FPSAnimator animator = new FPSAnimator(this, 60);
        animator.start();
    }

    public static void main(String[] args) {
        WindowManager.getInstance();
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        setCamera(gl, glu, 200);
        bullet.renderWithPhysics(gl);
        bullet.logic(gl);
        
    }
    
    //Setup the Camera
    private void setCamera(GL2 gl, GLU glu, float distance) {
        // Change to projection matrix.
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();

        // Perspective.
        float widthHeightRatio = (float) getWidth() / (float) getHeight();
        glu.gluPerspective(45, widthHeightRatio, 1, 1000);
        glu.gluLookAt(  //Where we are standing ("eye")
                        this.vertical, -180 + this.horizontal, distance,
                        //Where we are looking at ("at"): Directly at the center of the coordinate system.
                        bullet.getPinballPositionX(), -30 + bullet.getPinballPositionX(), bullet.getPinballPositionX(),         
                        //Where our head points into the sky ("up"): Directly up, along the Y coordinate
                        0, 1, 0);          

        // Change back to model view matrix.
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    //Setup the Light
    public void setLight(GL2 gl) {
        gl.glEnable(GL_COLOR_MATERIAL);
        gl.glEnable(GL2.GL_LIGHTING);
        
        float[] lightColorAmbient = { 0.0f, 0.0f, 0.0f, 1f };
        float[] lightColorSpecular = { 1f, 1f, 1f, 1f };

        //PunktLicht
        float[] lightPos = { -40, -50, 180, 1 };
        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_POSITION, lightPos, 0);
        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_AMBIENT, lightColorAmbient, 0);
        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_SPECULAR, lightColorSpecular, 0);
        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_DIFFUSE, lightColorSpecular, 0);
        gl.glEnable(GL2.GL_LIGHT1);
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        //Init GLU and GL
        gl = drawable.getGL().getGL2();
        glu = new GLU();
        //SetLight
        setLight(gl);
        //Create Bullet object
        bullet = new Bullet(gl);
        //Golbal Settings
        gl.glClearColor(0.5f, 0.5f, 0.5f, 0.5f); // set background (clear) color
        gl.glClearDepth(1.0f);      // set clear depth value to farthest
        gl.glEnable(GL2.GL_DEPTH_TEST); // enables depth testing
        gl.glDepthFunc(GL2.GL_LEQUAL);  // the type of depth test to do
        gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST); // best perspective correction
        gl.glShadeModel(GL2.GL_SMOOTH); // blends colors nicely, and smoothes out lighting
        gl.glEnable(GL2.GL_CULL_FACE);
        gl.glEnable(GL2.GL_NORMALIZE);
        //Music
        playSound();
    }

    //To setup the Backround Music and play it
    public void playSound() {
        try {
            Clip clip = AudioSystem.getClip();
            clip.open(AudioSystem.getAudioInputStream(new File("./resources/music/2.wav")));
            clip.loop(10);//Wird 10 mal wiederholt.
        } catch(IOException | LineUnavailableException | UnsupportedAudioFileException ex) {
            System.out.println("Error with playing sound." + ex.toString());
        }
    }
    
    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        //System.out.println("Key pressen: " + key);
        //Keyactions for the Menue
        if(bullet.getMenueStatus()){
            if (key == KeyEvent.VK_UP) {
                bullet.arrowNext();
            } else if (key == KeyEvent.VK_DOWN) {
                bullet.arrowNext();
            } else if (key == KeyEvent.VK_ENTER) {
                bullet.hitMenue();
            }
        } else{//Keyactions for the Game
            if (key == KeyEvent.VK_NUMPAD5) {
                horizontal -= 5;
            } else if (key == KeyEvent.VK_NUMPAD8) {
                horizontal += 5;
            } else if (key == KeyEvent.VK_NUMPAD4) {
                vertical -= 5;
            } else if (key == KeyEvent.VK_NUMPAD6) {
                vertical += 5;
            } else if (key == KeyEvent.VK_LEFT){
                setLeftFlipper(true);
            } else if (key == KeyEvent.VK_RIGHT){
                setRightFlipper(true);
            } else if (key == KeyEvent.VK_ENTER){
                resetBall();
            } else if (key == KeyEvent.VK_SPACE){
                pushBall();
            }
        }
        repaint();
    }
    
    //To reset the ball
    public void resetBall(){
        bullet.setResetBall();
    }
    
    //To push the Ball and start the game
    public void pushBall(){
        bullet.pushBall();
    }
    
    public void setLeftFlipper(boolean state){
        bullet.setActionLeft(state);
        //System.out.println("Left Flipper set "+state);
    }
    
    public void setRightFlipper(boolean state){
        bullet.setActionRight(state);
        //System.out.println("Right Flipper set "+state);
    }

    @Override
    public void keyTyped(KeyEvent e) {
        char ch = e.getKeyChar();

    }

    @Override
    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();
        if (key == KeyEvent.VK_LEFT){
            setLeftFlipper(false);
        } else if (key == KeyEvent.VK_RIGHT){
            setRightFlipper(false);
        }
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrtho(-100f, 100f, -100f, 100f, -200f, 200f);
        gl.glMatrixMode(GL2.GL_MODELVIEW);
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
    }

}
