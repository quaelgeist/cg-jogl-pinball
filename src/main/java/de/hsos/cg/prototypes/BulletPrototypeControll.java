/*
    Klasse zum Testen
*/
package de.hsos.cg.prototypes;

import de.hsos.cg.engine.*;
import com.bulletphysics.collision.broadphase.AxisSweep3;
import com.bulletphysics.collision.broadphase.BroadphaseInterface;
import com.bulletphysics.collision.broadphase.CollisionFilterGroups;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.CollisionFlags;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.collision.dispatch.PairCachingGhostObject;
import com.bulletphysics.collision.shapes.CapsuleShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.collision.shapes.SphereShape;
import com.bulletphysics.collision.shapes.StaticPlaneShape;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.dynamics.character.KinematicCharacterController;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.MotionState;
import com.bulletphysics.linearmath.Transform;
import de.hsos.cg.objects.flippers.LeftFlipper;
import de.hsos.cg.objects.Pinball;
import java.util.HashSet;
import java.util.Set;
import static javax.media.opengl.GL.GL_COLOR_BUFFER_BIT;
import static javax.media.opengl.GL.GL_DEPTH_BUFFER_BIT;
import javax.media.opengl.GL2;
import static javax.media.opengl.GL2GL3.GL_QUADS;
import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

/**
 *
 * @author sean
 */
public final class BulletPrototypeControll {

    private boolean actionLeft = false;
    private boolean actionRight = false;
    private boolean resetBall = false;
    

    float mass = 2F;
    float leftAngle = -2;
    private float leftFlipperHightness = 10;
    private float rightFlipperHightness = 10;

    private static final Transform DEFAULT_BALL_TRANSFORM = new Transform(new Matrix4f(new Quat4f(0, 0, 0, 1), new Vector3f(0, 35, 0), 1.0f));

    private Set<RigidBody> rigidElements = new HashSet<RigidBody>();

    private Transform worldTransform;

    private DefaultCollisionConfiguration collisionConfiguration;
    private CollisionDispatcher dispatcher;
    private SequentialImpulseConstraintSolver solver;
    private DiscreteDynamicsWorld dynamicsWorld;

    private DefaultMotionState groundMotionState;
    private RigidBodyConstructionInfo groundRigidBodyCI;

    private MotionState ballMotion;
    private MotionState flipperLeftMotion;
    private Vector3f fallInertia;

    private CollisionShape groundShape;
    private CollisionShape ballShape;
    private CollisionShape flipperRightShape;
    private CollisionShape plateShape;
    private ConvexShape flipperLeftShape;

    private KinematicMotionState flipperLeftMotionState;

    private RigidBodyConstructionInfo ballRigidBodyConsInfo;
    private RigidBodyConstructionInfo flipperRightBodyConsInfo;
    private RigidBodyConstructionInfo flipperLeftBodyConsInfo;

    private RigidBody groundRigidBody;
    private RigidBody ballRigidBody;
    private RigidBody flipperRightRigidBody;
    private RigidBody flipperLeftRigidBody;
    private RigidBody plateRigidBody;

    private KinematicCharacterController characterLeft;
    private PairCachingGhostObject ghostObjectLeft;
//    private GhostObject ghostObjectLeft;

    private BroadphaseInterface overlappingPairCache;

    
    Pinball pinball = null;
    LeftFlipper flipperLeft = null;

    public BulletPrototypeControll(GL2 gl) {
        worldTransform = new Transform();
        worldTransform.setIdentity();
        initPhysics();
        pinball = new Pinball(gl);
        flipperLeft = new LeftFlipper(gl);
    }

    public void initPhysics() {

        //The object that will roughly find out whether bodies are colliding.
//        broadphase = new DbvtBroadphase();
        collisionConfiguration = new DefaultCollisionConfiguration();

        //???
        Vector3f worldMin = new Vector3f(-1000f, -1000f, -1000f);
        Vector3f worldMax = new Vector3f(1000f, 1000f, 1000f);
        AxisSweep3 sweepBP = new AxisSweep3(worldMin, worldMax);
        overlappingPairCache = sweepBP;

        //The object that will accurately find out whether, when, how, and where bodies are colliding.
        dispatcher = new CollisionDispatcher(collisionConfiguration);

        //The object that will determine what to do after collision.
        solver = new SequentialImpulseConstraintSolver();

        //Initialise the JBullet world.
        dynamicsWorld = new DiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);

        //Set the gravity to 10 metres per second squared (m/s^2). Gravity affects all bodies with a mass larger thanC
        dynamicsWorld.setGravity(new Vector3f(0, -10, 0));

        //Initialise 'groundShape' to a static plane shape on the origin facing upwards ([0, 1, 0] is the normal).
        //0.25 metres is an added buffer between the ground and potential colliding bodies, used to prevent the bodies
        //from partially going through the floor. It is also possible to think of this as the plane being lifted 0.25m.
        groundShape = new StaticPlaneShape(new Vector3f(0, 1, 0), 0.25f);

        //Initialise 'groundMotionState' to a motion state that simply assigns the origin [0, 0, 0] as the origin of
        //the ground.
        groundMotionState = new DefaultMotionState(new Transform(new Matrix4f(new Quat4f(0, 0, 0, 1), new Vector3f(0, 0, 0), 1.0f)));

        //Initialise 'groundBodyConstructionInfo' to a value that contains the mass, the motion state, the shape, and the inertia (= resistance to change).
        groundRigidBodyCI = new RigidBodyConstructionInfo(0, groundMotionState, groundShape, new Vector3f(0, 0, 0));

        // Set the restitution, also known as the bounciness or spring, to 0.25. The restitution may range from 0.0
        // not bouncy) to 1.0 (extremely bouncy).
        groundRigidBodyCI.restitution = 1.f;

        // Initialise 'groundRigidBody', the final variable representing the ground, to a rigid body with the previously
        // assigned construction information.
        groundRigidBody = new RigidBody(groundRigidBodyCI);

        // Add the ground to the JBullet world.
        dynamicsWorld.addRigidBody(groundRigidBody);

        //ball
        //Initialise 'ballShape' to a sphere with a radius of 3 metres.
        ballShape = new SphereShape(3);
        //Initialise 'Motion' to a motion state that assigns a specified location.
        ballMotion = new DefaultMotionState(DEFAULT_BALL_TRANSFORM);
        //Calculate the inertia (resistance to movement) using the mass (2.5 kilograms).
        Vector3f ballInertia = new Vector3f(0, 0, 0);
        ballShape.calculateLocalInertia(mass, ballInertia);
        //Composes the construction info of its mass, its motion state, its shape, and its inertia.
        ballRigidBodyConsInfo = new RigidBodyConstructionInfo(mass, ballMotion, ballShape, ballInertia);
        //Set the restitution, also known as the bounciness or spring, to 0.5. The restitution may range from 0.0
        //not bouncy) to 1.0 (extremely bouncy).
        ballRigidBodyConsInfo.restitution = 0.5f;
        ballRigidBodyConsInfo.angularDamping = 0.95f;
        //now we add it to our physics simulation 
        ballRigidBody = new RigidBody(ballRigidBodyConsInfo);
        // Disable 'sleeping' for the controllable ball.
        ballRigidBody.setActivationState(CollisionObject.DISABLE_DEACTIVATION);
        //add object to the set of rigid elements
        rigidElements.add(ballRigidBody);
        //and register as collideable obcet
        dynamicsWorld.addRigidBody(ballRigidBody);
        
        //left flipper
        //Kinematic object are 'static' 'moveable' without mass or Gravits
        flipperLeftMotionState = new KinematicMotionState();

        Transform flipperLeftTransform = new Transform(new Matrix4f(new Quat4f(-1, -2, 0, 0), new Vector3f(0, leftFlipperHightness, 0), 1.0f));

        flipperLeftMotionState.setWorldTransform(flipperLeftTransform);
        //initialize the shape
//        flipperLeftShape = new BoxShape(new Vector3f(6, 4, 5));
        flipperLeftShape = new CapsuleShape(6, 20);
        //add the rig without mass
        flipperLeftRigidBody = new RigidBody(0, flipperLeftMotionState, flipperLeftShape);
        //add an 'ghost' object
        ghostObjectLeft = new PairCachingGhostObject();
        //combine the ghost object and the shape to a character
        characterLeft = new KinematicCharacterController(ghostObjectLeft, flipperLeftShape, 1.0F);

        //set collision flags for the Rig
        flipperLeftRigidBody.setCollisionFlags(flipperLeftRigidBody.getCollisionFlags() | CollisionFlags.KINEMATIC_OBJECT);
        //disable sleep state. Sleeping objects are never moving again.
        flipperLeftRigidBody.setActivationState(CollisionObject.DISABLE_DEACTIVATION);

        //add the rig to the collectio
        rigidElements.add(flipperLeftRigidBody);

        //give the ghost object the same collision-shape as the rig has
        ghostObjectLeft.setCollisionShape(flipperLeftShape);
        //define the ghost as character, to make it moveable
        ghostObjectLeft.setCollisionFlags(CollisionFlags.CHARACTER_OBJECT);

        //add ghost to world
        dynamicsWorld.addCollisionObject(ghostObjectLeft, CollisionFilterGroups.CHARACTER_FILTER, (short) (CollisionFilterGroups.STATIC_FILTER | CollisionFilterGroups.DEFAULT_FILTER));
        //define collideable action
        dynamicsWorld.addAction(characterLeft);

        //add rig to world
        dynamicsWorld.addRigidBody(flipperLeftRigidBody);

    }
    
//    public void drawShapes(GL2 gl, Vector3f from, Vector3f to){
//        
//        gl.glLineWidth(2F);
//        gl.glColor3f(1F, 0F, 0F);
//        gl.glBegin(GL_LINES);
//            gl.glVertex3f(from.x, from.y, from.z);
//            gl.glVertex3f(from.x, from.y, from.z);
//        gl.glEnd();
//    }

    public void renderWithPhysics(GL2 gl) {
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        for (RigidBody body : rigidElements) {

            gl.glPushMatrix();
            gl.glColor3f(0.0f, 0.0f, 0.0f);
            Vector3f elementPosition = body.getWorldTransform(new Transform()).origin;
            gl.glTranslatef(elementPosition.x, elementPosition.y, elementPosition.z);

            if (body.equals(ballRigidBody)) {
                pinball.drawModel(gl);
            } else if (body.equals(flipperLeftRigidBody)) {
                if (actionLeft == false) {
                    gl.glRotatef(-90f, 1f, 0f, 0f);
                    gl.glRotatef(45f, 0f, 1f, 0f);
                } else {
                    gl.glRotatef(-90f, 1f, 0f, 0f);
                    gl.glRotatef(-45f, 0f, 1f, 0f);
                }
                flipperLeft.drawModel(gl);
            }

            gl.glPopMatrix();
//            dynamicsWorld.debugDrawWorld();
        }

//         Draw the floor.
        gl.glBegin(GL_QUADS);

        gl.glColor3f(0.6f, 0.6f, 0.6f);
        gl.glVertex3f(-500f, 0f, -500f);
        gl.glVertex3f(-500f, 0f, +500f);
        gl.glVertex3f(+500f, 0f, +500f);
        gl.glVertex3f(+500f, 0f, -500f);

        gl.glEnd();
    }

    public void logic(GL2 gl) {
        // Reset the model-view matrix.
        gl.glLoadIdentity();
        // Runs the JBullet physics simulation for the specified time in seconds.
        dynamicsWorld.stepSimulation(1f, 5);

        if (actionLeft == true) {
            addHighnessLeft(true);

        } else {
            addHighnessLeft(false);
        }
        if (actionRight == true) {

        } else {

        }

        if (resetBall) {
            resetBall();
        }
        // For every element
//        for (RigidBody body : rigidElements) {
//            // Retrieve the ball's position from the JBullet body object.
//            Vector3f position = body.getMotionState().getWorldTransform(new Transform()).origin;
//            
//        }
    }

    public void addHighnessLeft(boolean state) {

        Transform T;
        if (state) {
//            if (leftAngle < 2 ){
//                leftAngle += 0.1F;
//            }
            T = new Transform(new Matrix4f(new Quat4f(-1, leftAngle, 0, 0), new Vector3f(0, leftFlipperHightness, 0), 1.0f));

        } else {
//            if (leftAngle > -2){
//                leftAngle -= 0.1F;
//            }
            T = new Transform(new Matrix4f(new Quat4f(-1, -leftAngle, 0, 0), new Vector3f(0, leftFlipperHightness, 0), 1.0f));
        }
        flipperLeftMotionState.setWorldTransform(T);
        flipperLeftRigidBody.setCenterOfMassTransform(T);
        
    }

    private void resetBall() {
        ballRigidBody.setCenterOfMassTransform(DEFAULT_BALL_TRANSFORM);
        ballRigidBody.setAngularVelocity(new Vector3f(0, 0, 0));
        ballRigidBody.setLinearVelocity(new Vector3f(0, 0, 0));
        resetBall = false;
    }

    public CollisionShape getBallShape() {
        return ballShape;
    }

    public void setBallShape(CollisionShape ballShape) {
        this.ballShape = ballShape;
    }

    public MotionState getBallMotion() {
        return ballMotion;
    }

    public void setBallMotion(MotionState ballMotion) {
        this.ballMotion = ballMotion;
    }

    public DefaultCollisionConfiguration getCollisionConfiguration() {
        return collisionConfiguration;
    }

    public void setCollisionConfiguration(DefaultCollisionConfiguration collisionConfiguration) {
        this.collisionConfiguration = collisionConfiguration;
    }

    public CollisionDispatcher getDispatcher() {
        return dispatcher;
    }

    public void setDispatcher(CollisionDispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    public SequentialImpulseConstraintSolver getSolver() {
        return solver;
    }

    public void setSolver(SequentialImpulseConstraintSolver solver) {
        this.solver = solver;
    }

    public DiscreteDynamicsWorld getDynamicsWorld() {
        return dynamicsWorld;
    }

    public void setDynamicsWorld(DiscreteDynamicsWorld dynamicsWorld) {
        this.dynamicsWorld = dynamicsWorld;
    }

    public CollisionShape getGroundShape() {
        return groundShape;
    }

    public void setGroundShape(CollisionShape groundShape) {
        this.groundShape = groundShape;
    }

    public CollisionShape getFallShape() {
        return ballShape;
    }

    public void setFallShape(CollisionShape fallShape) {
        this.ballShape = fallShape;
    }

    public DefaultMotionState getGroundMotionState() {
        return groundMotionState;
    }

    public void setGroundMotionState(DefaultMotionState groundMotionState) {
        this.groundMotionState = groundMotionState;
    }

    public RigidBodyConstructionInfo getGroundRigidBodyCI() {
        return groundRigidBodyCI;
    }

    public void setGroundRigidBodyCI(RigidBodyConstructionInfo groundRigidBodyCI) {
        this.groundRigidBodyCI = groundRigidBodyCI;
    }

    public RigidBody getGroundRigidBody() {
        return groundRigidBody;
    }

    public void setGroundRigidBody(RigidBody groundRigidBody) {
        this.groundRigidBody = groundRigidBody;
    }

    public RigidBodyConstructionInfo getBallRigidBodyConsInfo() {
        return ballRigidBodyConsInfo;
    }

    public void setBallRigidBodyConsInfo(RigidBodyConstructionInfo ballRigidBodyConsInfo) {
        this.ballRigidBodyConsInfo = ballRigidBodyConsInfo;
    }

    public Vector3f getFallInertia() {
        return fallInertia;
    }

    public void setFallInertia(Vector3f fallInertia) {
        this.fallInertia = fallInertia;
    }

    public RigidBody getBallRigidBody() {
        return ballRigidBody;
    }

    public void setBallRigidBody(RigidBody ballRigidBody) {
        this.ballRigidBody = ballRigidBody;
    }
    
    public boolean isActionLeft() {
        return actionLeft;
    }

    public void setActionLeft(boolean actionLeft) {
        this.actionLeft = actionLeft;
    }

    public boolean isActionRight() {
        return actionRight;
    }

    public void setActionRight(boolean actionRight) {
        this.actionRight = actionRight;
    }

    public Set<RigidBody> getRigidElements() {
        return rigidElements;
    }

    public void setRigidElements(Set<RigidBody> rigidElements) {
        this.rigidElements = rigidElements;
    }

    public MotionState getFlipperLeftMotion() {
        return flipperLeftMotion;
    }

    public void setFlipperLeftMotion(MotionState flipperLeftMotion) {
        this.flipperLeftMotion = flipperLeftMotion;
    }

    public CollisionShape getFlipperRightShape() {
        return flipperRightShape;
    }

    public void setFlipperRightShape(CollisionShape flipperRightShape) {
        this.flipperRightShape = flipperRightShape;
    }

    public CollisionShape getFlipperLeftShape() {
        return flipperLeftShape;
    }

    public CollisionShape getPlateShape() {
        return plateShape;
    }

    public void setPlateShape(CollisionShape plateShape) {
        this.plateShape = plateShape;
    }

    public RigidBodyConstructionInfo getFlipperRightBodyConsInfo() {
        return flipperRightBodyConsInfo;
    }

    public void setFlipperRightBodyConsInfo(RigidBodyConstructionInfo flipperRightBodyConsInfo) {
        this.flipperRightBodyConsInfo = flipperRightBodyConsInfo;
    }

    public RigidBodyConstructionInfo getFlipperLeftBodyConsInfo() {
        return flipperLeftBodyConsInfo;
    }

    public void setFlipperLeftBodyConsInfo(RigidBodyConstructionInfo flipperLeftBodyConsInfo) {
        this.flipperLeftBodyConsInfo = flipperLeftBodyConsInfo;
    }

    public RigidBody getFlipperRightRigidBody() {
        return flipperRightRigidBody;
    }

    public void setFlipperRightRigidBody(RigidBody flipperRightRigidBody) {
        this.flipperRightRigidBody = flipperRightRigidBody;
    }

    public RigidBody getFlipperLeftRigidBody() {
        return flipperLeftRigidBody;
    }

    public void setFlipperLeftRigidBody(RigidBody flipperLeftRigidBody) {
        this.flipperLeftRigidBody = flipperLeftRigidBody;
    }

    public RigidBody getPlateRigidBody() {
        return plateRigidBody;
    }

    public void setPlateRigidBody(RigidBody plateRigidBody) {
        this.plateRigidBody = plateRigidBody;
    }

    public Pinball getPinball() {
        return pinball;
    }

    public void setPinball(Pinball pinball) {
        this.pinball = pinball;
    }

    public LeftFlipper getFlipperLeft() {
        return flipperLeft;
    }

    public void setFlipperLeft(LeftFlipper flipperLeft) {
        this.flipperLeft = flipperLeft;
    }

    public boolean isResetBall() {
        return resetBall;
    }

    public void setResetBall(boolean resetBall) {
        this.resetBall = resetBall;
    }

    public float getLeftFlipperHightness() {
        return leftFlipperHightness;
    }

    public void setLeftFlipperHightness(float leftFlipperHightness) {
        this.leftFlipperHightness = leftFlipperHightness;
    }

    public float getRightFlipperHightness() {
        return rightFlipperHightness;
    }

    public void setRightFlipperHightness(float rightFlipperHightness) {
        this.rightFlipperHightness = rightFlipperHightness;
    }
}
