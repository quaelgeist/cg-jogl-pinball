/*
    For testing
*/
package de.hsos.cg.prototypes;

import de.hsos.cg.objects.obstacles.Bumper;
import de.hsos.cg.objects.flippers.RightFlipper;
import de.hsos.cg.objects.flippers.LeftFlipper;
import com.jogamp.opengl.util.FPSAnimator;
import de.hsos.cg.objects.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.awt.GLJPanel;
import static javax.media.opengl.fixedfunc.GLLightingFunc.GL_COLOR_MATERIAL;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;

@SuppressWarnings("serial")
public class draw_Pinball_Game extends GLJPanel implements GLEventListener, KeyListener {

    public static int width = 1000;
    public static int height = 1000;
    public float horizontal = 60;
    public float vertical = 0;
    public Border border = null;
    public Bumper bumper1 = null;
    public Bumper bumper2 = null;
    public Bumper bumper3 = null;
    public Initiator initiator = null;
    public LeftFlipper lFlipper = null;
    public RightFlipper rFlipper = null;
    
    public draw_Pinball_Game() {

        setFocusable(true);
        addGLEventListener(this);
        addKeyListener(this);
        FPSAnimator animator = new FPSAnimator(this, 60);
        animator.start();
        
    }

    public static void main(String[] args) {
        JFrame window = new JFrame();
        window.getContentPane().add(new draw_Pinball_Game());
        window.setSize(width, height);
        window.setVisible(true);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        render(gl);
    }

    public void setLight(GL2 gl) {
        gl.glEnable(GL_COLOR_MATERIAL);
        gl.glEnable(GL2.GL_LIGHTING);
        
        float[] lightColorAmbient = { 0.0f, 0.0f, 0.0f, 1f };
        float[] lightColorSpecular = { 1f, 1f, 1f, 1f }; //Fast schwarz, spiegelt fast garnicht

        // Set light parameters.
        //PunktLicht
        float[] lightPos = { 40, -50, 180, 1 };
        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_POSITION, lightPos, 0);
        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_AMBIENT, lightColorAmbient, 0);
        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_SPECULAR, lightColorSpecular, 0);
        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_DIFFUSE, lightColorSpecular, 0);
        gl.glEnable(GL2.GL_LIGHT1);

    }

    @Override
    public void init(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();

        gl.glClearColor(0.5f, 0.5f, 0.5f, 0.5f); // set background (clear) color
        gl.glClearDepth(1.0f);      // set clear depth value to farthest
        gl.glEnable(GL2.GL_DEPTH_TEST); // enables depth testing
        gl.glDepthFunc(GL2.GL_LEQUAL);  // the type of depth test to do
        gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST); // best perspective correction
        gl.glShadeModel(GL2.GL_SMOOTH); // blends colors nicely, and smoothes out lighting
        gl.glEnable(GL2.GL_CULL_FACE);
        gl.glEnable(GL2.GL_NORMALIZE);

        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrtho(-100f, 100f, -100f, 100f, -200f, 200f);
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        
        setLight(gl);
        border = new Border(gl);
        bumper1 = new Bumper(gl, -30, 1, -30);
        bumper2 = new Bumper(gl, 20, 1, -30);
        bumper3 = new Bumper(gl, -5, 1, -60);
        initiator = new Initiator(gl);
//        lFlipper = new LeftFlipper(gl, -18, 0, 88);
//        rFlipper = new RightFlipper(gl, 8, 0, 88);
    }

    private void render(GL2 gl) {
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
        gl.glPushMatrix();
        //************************************//
        
        reInit(gl);
        gl.glTranslatef(border.getxLocation(), border.getyLocation(), border.getzLocation());
        border.drawModel(gl);
        
        reInit(gl);
        gl.glTranslatef(bumper1.getxLocation(), bumper1.getyLocation(), bumper1.getzLocation());
        bumper1.drawModel(gl);
        
        reInit(gl);
        gl.glTranslatef(bumper2.getxLocation(), bumper2.getyLocation(), bumper2.getzLocation());
        bumper2.drawModel(gl);
        
        reInit(gl);
        gl.glTranslatef(bumper3.getxLocation(), bumper3.getyLocation(), bumper3.getzLocation());
        bumper3.drawModel(gl);

        reInit(gl);
        gl.glTranslatef(initiator.getxLocation(), initiator.getyLocation(), initiator.getzLocation());
        initiator.drawModel(gl);
        
        reInit(gl);
        gl.glTranslatef(lFlipper.getxLocation(), lFlipper.getyLocation(), lFlipper.getzLocation());
        gl.glRotatef(- 10, 0f, 1f, 0f);
        lFlipper.drawModel(gl);
        
        reInit(gl);
        gl.glTranslatef(rFlipper.getxLocation(), rFlipper.getyLocation(), rFlipper.getzLocation());
        gl.glRotatef(10, 0f, 1f, 0f);
        rFlipper.drawModel(gl);
        //************************************//
        gl.glPopMatrix();
        gl.glFlush();
    }

    public void reInit(GL2 gl){
        gl.glLoadIdentity();
        float scal = 0.7f;
        gl.glScalef(scal, scal, scal);
        gl.glRotatef(horizontal, 1f, 0f, 0f);
        gl.glRotatef(vertical, 0f, 1f, 0f);
    }
    
    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();  // Tells which key was pressed.
        System.out.println("Key pressen: " + key);
        if (key == KeyEvent.VK_NUMPAD5) {
            horizontal -= 5;
        } else if (key == KeyEvent.VK_NUMPAD8) {
            horizontal += 5;
        } else if (key == KeyEvent.VK_NUMPAD4) {
            vertical -= 5;
        } else if (key == KeyEvent.VK_NUMPAD6) {
            vertical += 5;
        } else {
            System.out.println("typed: " + key);
        }

        repaint();
    }

    @Override
    public void keyTyped(KeyEvent e) {
        char ch = e.getKeyChar();

    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

        GL2 gl = drawable.getGL().getGL2();

        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        GLU glu = new GLU();
        gl.glOrtho(-100f, 100f, -100f, 100f, -200f, 200f);
        gl.glMatrixMode(GL2.GL_MODELVIEW);
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
    }
}
