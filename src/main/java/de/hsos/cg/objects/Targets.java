///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package de.hsos.cg.objects;
//
//import de.hsos.cg.objects.loader.*;
//import javax.media.opengl.GL2;
//
///**
// *
// * @author Jochen Gildner
// */
//public class Targets {
//    private GLModel targets = null;
//    private float xLocation = 0;
//    private float yLocation = 0;
//    private float zLocation = 0;
//    boolean target1 = true;
//    boolean target2 = true;
//    boolean target3 = true;
//    boolean target4 = true;
//    boolean target5 = true;
//    
//    public Targets(GL2 gl) {
//        modelPath = "./resources/models/Ziele.obj";
//        texturePath = "./resources/models/Material.mtl";
//        loadModel(gl);
//    }
//    
//    public Targets(GL2 gl, float xLocation, float yLocation, float zLocation) {
//        this.xLocation = xLocation;
//        this.yLocation = yLocation;
//        this.zLocation = zLocation;
//        targets = ModelLoaderOBJ.LoadModel("./resources/models/Ziele.obj", "./resources/models/Material.mtl", gl);
//    }
//    
//    public float getxLocation() {
//        return xLocation;
//    }
//
//    public float getyLocation() {
//        return yLocation;
//    }
//
//    public float getzLocation() {
//        return zLocation;
//    }
//    
//    public void drawModel(GL2 gl){
//        targets.opengldraw(gl);
//    }
//    
//    public void hitTarget1(){
//        this.target1 = false;
//        if (this.checkTargets())
//            returnColors();
//        
//    }
//    
//    public boolean checkTargets(){
//        if(target1 == false && target2 == false && target3 == false && target4 == false && target5 == false){
//            target1 = target2 = target3 = target4 = target5 = true;
//            return true;
//        }
//        return false;
//    }
//    
//    public void returnColors(){
//        
//    }
//    
//    /*public void testing(){
//        System.out.println(targets.getMaterials().toString());
//    }*/
//}
