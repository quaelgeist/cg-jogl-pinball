/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.hsos.cg.objects.flippers;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.ConvexHullShape;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.Transform;
import de.hsos.cg.objects.abstracts.BasicFlipperObject;
import de.hsos.cg.objects.loader.VertexLoader;
import javax.media.opengl.GL2;
import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

/**
 *
 * @author Jochen Gildner
 */

public class LeftFlipper extends BasicFlipperObject{
    
    
//    private final float angle = -2;
    private Quat4f angle = new Quat4f(0, 0, 0, 1);
    private final float qUp = 0.35f;
    private final float qDown = -0.35f;
    
    
    
    public LeftFlipper(GL2 gl) {
        modelPath = "./resources/models/Arm_Links.obj";
        texturePath = "./resources/models/Material.mtl";
        loadModel(gl);
        yLocation = super.localY;
        defaultPosition = new Transform(new Matrix4f(angle, new Vector3f(xLocation, yLocation, zLocation), 1.0f));
        quaternialUpState = qUp;
        quaternialDownState = qDown;
        upDir = new Vector3f(0, 0, 1);
        downDir = new Vector3f(0, 0, -1);
        super.initialize();
    }
    
    public LeftFlipper(GL2 gl, float xLocation) {
        this.xLocation = xLocation;
        modelPath = "./resources/models/Arm_Links.obj";
        texturePath = "./resources/models/Material.mtl";
        loadModel(gl);
        yLocation = this.localY;
        defaultPosition = new Transform(new Matrix4f(angle, new Vector3f(xLocation, yLocation, zLocation), 1.0f));
        quaternialUpState = qUp;
        quaternialDownState = qDown;
        upDir = new Vector3f(0, 0, super.getMovementSpeed());
        downDir = new Vector3f(0, 0, -super.getMovementSpeed());
        super.initialize();
    }

    @Override
    public boolean reachedLimitUp(float rotation) {
        return rotation < this.getQuaternialUpState();
    }

    @Override
    public boolean reachedLimitDown(float rotation) {
        return rotation > this.getQuaternialDownState();
    }

    
}
