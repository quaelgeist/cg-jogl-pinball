package de.hsos.cg.objects.loader;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.media.opengl.GL2;

/**
 * Image loading class that converts BufferedImages into a data structure that
 * can be easily passed to OpenGL.
 * 
 * @author Pepijn Van Eeckhoudt Downloaded from:
 *         http://www.felixgers.de/teaching/jogl/
 */

//ModelLoaderOBJ from "Praktikum"
public class ModelLoaderOBJ {

	public static GLModel LoadModel(String objPath, String mtlPath, GL2 gl){
		
		GLModel model = null;
		try {
                    BufferedReader b_read1;
                    try (FileInputStream r_path1 = new FileInputStream(objPath)) {
                        b_read1 = new BufferedReader(new InputStreamReader(
                                r_path1));
                        model = new GLModel(b_read1, true,
                                mtlPath, gl);
                    }
			b_read1.close();

		} catch (IOException e) {
			System.out.println("LOADING ERROR" + e);
		}
		return model;
	}
}