
package de.hsos.cg.objects.loader;

import com.bulletphysics.util.ObjectArrayList;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import javax.vecmath.Vector3f;

/**
 *
 * @author Coonie
 */
public class VertexLoader {
    private ObjectArrayList<Vector3f> vecArr = new ObjectArrayList<>();
    
    //Get the *.OBJ and get the Vertexes of it, converting them into a Vector Array for the Convex Hulls
    public ObjectArrayList<Vector3f> getVecArr(String objPath) {
        try{
            try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(objPath)))) {
                String newline;
                while ((newline = br.readLine()) != null) {
                    if (newline.length() > 0) {
                        newline = newline.trim();
                        if (newline.startsWith("v ")) {
                            float coords[] = new float[3];
                            newline = newline.substring(2, newline.length());
                            StringTokenizer st = new StringTokenizer(newline, " ");
                            for (int i = 0; st.hasMoreTokens(); i++) {
                                coords[i] = Float.parseFloat(st.nextToken());
                            }
                            vecArr.add(new Vector3f(coords[0], coords[1], coords[2]));
                        }
                    }
                }
                br.close();
            }
        } catch (IOException e) {
            System.out.println("Failed to read file");
        } catch (NumberFormatException e) {
            System.out.println("Malformed OBJ file");
        }
        //To see the content of the Array
        /*for (int i = 0; i < vecArr.size(); i++) {
            System.out.println(vecArr.get(i).x + ", " + vecArr.get(i).y + ", " + vecArr.get(i).z);
        }*/
        return vecArr;
    } 
}
