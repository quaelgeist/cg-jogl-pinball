/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.hsos.cg.objects.obstacles;

import com.bulletphysics.collision.shapes.ConvexHullShape;
import de.hsos.cg.objects.abstracts.BasicGraphicalObject;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.Transform;
import de.hsos.cg.objects.loader.*;
import javax.media.opengl.GL2;
import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

/**
 *
 * @author Jochen Gildner
 */
public class RightBevel  extends BasicGraphicalObject{
    
    public RightBevel(GL2 gl, float xLocation, float yLocation, float zLocation) {
        this.xLocation = xLocation;
        this.yLocation = yLocation;
        this.zLocation = zLocation;
        modelPath = "./resources/models/Begrenzung_Rechts_2.obj";
        texturePath = "./resources/models/Material.mtl";
        loadModel(gl);
        initialize();
    }
    
    @Override
    public void initialize(){
        shape = new ConvexHullShape(new VertexLoader().getVecArr(modelPath));
        motion = new DefaultMotionState(new Transform(new Transform(new Matrix4f(new Quat4f(0, 0, 0, 1), new Vector3f(xLocation, yLocation, zLocation), 1.0f))));
        constructionInfo = new RigidBodyConstructionInfo(0, motion, shape);

        constructionInfo.restitution = 0.5f;
        constructionInfo.angularDamping = 0.5f;
        rig = new RigidBody(constructionInfo);
    }
}
