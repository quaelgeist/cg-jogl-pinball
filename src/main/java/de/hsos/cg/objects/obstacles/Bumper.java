/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.hsos.cg.objects.obstacles;

import de.hsos.cg.objects.abstracts.BasicGraphicalObject;
import com.bulletphysics.collision.shapes.SphereShape;
import com.bulletphysics.dynamics.RigidBody; 
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.Transform;
import javax.media.opengl.GL2;
import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

/**
 *
 * @author Jochen Gildner
 */
public class Bumper  extends BasicGraphicalObject{
    
    public Bumper(GL2 gl, float xLocation, float yLocation, float zLocation) {
        this.xLocation = xLocation;
        this.yLocation = yLocation;
        this.zLocation = zLocation;
        modelPath = "./resources/models/Bumper.obj";
        texturePath = "./resources/models/Material.mtl";
        loadModel(gl);
        initialize();
    }
    
    @Override
    public void initialize(){
        shape = new SphereShape(5);
        motion = new DefaultMotionState(new Transform(new Transform(new Matrix4f(new Quat4f(0, 0, 0, 1), new Vector3f(xLocation, yLocation, zLocation), 1.0f))));
        constructionInfo = new RigidBodyConstructionInfo(0, motion, shape);
        constructionInfo.restitution = 3.0f;
        constructionInfo.angularDamping = 2.0f;
        rig = new RigidBody(constructionInfo);
    }
    
    
}
