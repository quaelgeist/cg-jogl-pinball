
package de.hsos.cg.objects.abstracts;

import com.bulletphysics.collision.dispatch.CollisionFlags;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.PairCachingGhostObject;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.ConvexHullShape;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.dynamics.character.KinematicCharacterController;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.Transform;
import de.hsos.cg.engine.KinematicMotionState;
import de.hsos.cg.objects.loader.VertexLoader;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

/**
 *
 * @author sean
 */
public abstract class BasicFlipperObject extends BasicGraphicalObject{
    
    protected float angle;
    protected float quaternialUpState;
    protected float quaternialDownState;
    protected final float mass = 5000;
    protected final float movementSpeed = 500000;
    protected final float localY = -95;
    protected Vector3f upDir;
    protected Vector3f downDir;
    protected float yOffset = 5;
    
    protected Transform defaultPosition;
    
    @Override
    public void initialize(){
//        shape = new ConvexHullShape(new VertexLoader().getVecArr(modelPath));
        shape = new BoxShape(new Vector3f(8, 2, 3));
        motion = new DefaultMotionState(defaultPosition);
        Vector3f intertia = new Vector3f(0, 0, 0);
        shape.calculateLocalInertia(mass, intertia);
        constructionInfo = new RigidBodyConstructionInfo(mass, motion, shape, intertia);
        constructionInfo.restitution = 1.0f;
        constructionInfo.angularDamping = 0.5f;
        rig = new RigidBody(constructionInfo);
        rig.setActivationState(CollisionObject.DISABLE_DEACTIVATION);
    }
    
    //returns true if the maximal limit is reached
    public abstract boolean reachedLimitUp(float rotation);
    
    public abstract boolean reachedLimitDown(float rotation);
    
    public Transform getDefaultPosition() {
        return defaultPosition;
    }
    
    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public void setShape(ConvexShape shape) {
        this.shape = shape;
    }

    public void setMotion(KinematicMotionState motion) {
        this.motion = motion;
    }

    public float getQuaternialUpState() {
        return quaternialUpState;
    }

    public float getQuaternialDownState() {
        return quaternialDownState;
    }

    public Vector3f getUpDir() {
        return upDir;
    }

    public Vector3f getDownDir() {
        return downDir;
    }

    public float getMovementSpeed() {
        return movementSpeed;
    }

    public float getyOffset() {
        return yOffset;
    }

    
    
    
    
    
}
