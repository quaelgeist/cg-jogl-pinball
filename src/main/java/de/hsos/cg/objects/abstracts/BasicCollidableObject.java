
package de.hsos.cg.objects.abstracts;

import com.bulletphysics.collision.dispatch.PairCachingGhostObject;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.MotionState;
import com.bulletphysics.linearmath.Transform;

/**
 *
 * @author sean
 */
public abstract class BasicCollidableObject {
//    protected Transform defaultPosition;
    protected CollisionShape shape;
    protected MotionState motion;
    protected RigidBody rig;
    protected PairCachingGhostObject ghost;
    protected RigidBodyConstructionInfo constructionInfo;
    protected float xLocation;
    protected float yLocation;
    protected float zLocation;

    public CollisionShape getShape() {
        return shape;
    }

    public void setShape(CollisionShape shape) {
        this.shape = shape;
    }

    public MotionState getMotion() {
        return motion;
    }

    public void setMotion(MotionState motion) {
        this.motion = motion;
    }

    public RigidBody getRig() {
        return rig;
    }

    public void setRig(RigidBody rig) {
        this.rig = rig;
    }

    public RigidBodyConstructionInfo getConstructionInfo() {
        return constructionInfo;
    }

    public void setConstructionInfo(RigidBodyConstructionInfo constructionInfo) {
        this.constructionInfo = constructionInfo;
    }

    public float getxLocation() {
        return xLocation;
    }

    public float getyLocation() {
        return yLocation;
    }

    public float getzLocation() {
        return zLocation;
    }

    public void setxLocation(float xLocation) {
        this.xLocation = xLocation;
    }

    public void setyLocation(float yLocation) {
        this.yLocation = yLocation;
    }

    public void setzLocation(float zLocation) {
        this.zLocation = zLocation;
    }

    public PairCachingGhostObject getGhost() {
        return ghost;
    }

    public void setGhost(PairCachingGhostObject ghost) {
        this.ghost = ghost;
    }
    
    
}
