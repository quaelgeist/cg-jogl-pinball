
package de.hsos.cg.objects.abstracts;

import de.hsos.cg.objects.loader.GLModel;
import de.hsos.cg.objects.loader.ModelLoaderOBJ;
import javax.media.opengl.GL2;

/**
 *
 * @author sean / jochen
 */
public abstract class BasicGraphicalObject extends BasicCollidableObject{
    
    protected String modelPath;
    protected String texturePath;
    protected GLModel model;
            
    public abstract void initialize();
    
    public void drawModel(GL2 gl){
        if (! (this.getModel() == null)){
            this.getModel().opengldraw(gl);
        }   
    }
    
    public void loadModel(GL2 gl){
        model = ModelLoaderOBJ.LoadModel(modelPath, texturePath, gl);
    }

    public String getModelPath() {
        return modelPath;
    }

    public void setModelPath(String modelPath) {
        this.modelPath = modelPath;
    }

    public String getTexturePath() {
        return texturePath;
    }

    public void setTexturePath(String texturePath) {
        this.texturePath = texturePath;
    }

    public GLModel getModel() {
        return model;
    }

    public void setModel(GLModel model) {
        this.model = model;
    } 
}
