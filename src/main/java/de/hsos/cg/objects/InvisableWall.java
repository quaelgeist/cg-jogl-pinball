
package de.hsos.cg.objects;

import de.hsos.cg.objects.abstracts.BasicCollidableObject;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

/**
 *
 * @author sean
 */
public class InvisableWall extends BasicCollidableObject{
    
    private float xLength;
    private float yLength;
    private float zLength;
    private float xSlant;
    private float ySlant;
    private float zSlant;
    private float kSlant;

    public InvisableWall(float xLocation, float yLocation, float zLocation, float xLength, float yLength, float zLength) {
        this.xLocation = xLocation;
        this.yLocation = yLocation;
        this.zLocation = zLocation;
        this.xLength = xLength;
        this.yLength = yLength;
        this.zLength = zLength;
        this.xSlant = 0;
        this.ySlant = 0;
        this.zSlant = 0;
        this.kSlant = 1;
        initialize();
    }

    public InvisableWall(float xLocation, float yLocation, float zLocation, float xLength, float yLength, float zLength, float xSlant, float ySlant, float zSlant, float kSlant) {
        this.xLocation = xLocation;
        this.yLocation = yLocation;
        this.zLocation = zLocation;
        this.xLength = xLength;
        this.yLength = yLength;
        this.zLength = zLength;
        this.xSlant = xSlant;
        this.ySlant = ySlant;
        this.zSlant = zSlant;
        this.kSlant = kSlant;
        initialize();
    }
    
    public void initialize(){
        shape = new BoxShape(new Vector3f(xLength, yLength, zLength));
        motion = new DefaultMotionState(new Transform(new Transform(new Matrix4f(new Quat4f(xSlant, ySlant, zSlant, kSlant), new Vector3f(xLocation, yLocation, zLocation), 1.0f))));
        constructionInfo = new RigidBodyConstructionInfo(0, motion, shape);
        constructionInfo.restitution = 0.5f;
        constructionInfo.angularDamping = 0f;
        rig = new RigidBody(constructionInfo);
    }

    public float getxLength() {
        return xLength;
    }

    public void setxLength(float xLength) {
        this.xLength = xLength;
    }

    public float getxSlant() {
        return xSlant;
    }

    public void setxSlant(float xSlant) {
        this.xSlant = xSlant;
    }

    public float getySlant() {
        return ySlant;
    }

    public void setySlant(float ySlant) {
        this.ySlant = ySlant;
    }

    public float getyLength() {
        return yLength;
    }

    public void setyLength(float yLength) {
        this.yLength = yLength;
    }

    public float getzLength() {
        return zLength;
    }

    public void setzLength(float zLength) {
        this.zLength = zLength;
    }

    public float getzSlant() {
        return zSlant;
    }

    public void setzSlant(float zSlant) {
        this.zSlant = zSlant;
    }

    public float getkSlant() {
        return kSlant;
    }

    public void setkSlant(float kSlant) {
        this.kSlant = kSlant;
    }
    
    
}
