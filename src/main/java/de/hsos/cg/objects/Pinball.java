/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.hsos.cg.objects;

import de.hsos.cg.objects.abstracts.BasicGraphicalObject;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.shapes.SphereShape;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.Transform;
import javax.media.opengl.GL2;
import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

/**
 *
 * @author sean
 */
public class Pinball extends BasicGraphicalObject{
    
    final float mass = 0.7F;
    final float maxSpeed = 150F;
    
    private static final Transform DEFAULT_BALL_TRANSFORM = new Transform(new Matrix4f(new Quat4f(0, 0, 0, 1), new Vector3f(54, -100, 0), 1.0f));
//    private static final Transform DEFAULT_BALL_TRANSFORM = new Transform(new Matrix4f(new Quat4f(0, 0, 0, 1), new Vector3f(5, 30, 0), 1.0f));

    
    public Pinball(GL2 gl){
        modelPath = "./resources/models/Kugel.obj";
        texturePath = "./resources/models/Material.mtl";
        loadModel(gl);
        initialize();
    }
    
    public void resetBall() {
        rig.setCenterOfMassTransform(DEFAULT_BALL_TRANSFORM);
        rig.setAngularVelocity(new Vector3f(0, 0, 0));
        rig.setLinearVelocity(new Vector3f(0, 0, 0));
    }
    
    public void pushBall(){
        rig.applyCentralForce(new Vector3f(0, 700, 0));
    }

    @Override
    public void initialize() {
        //Initialise 'ballShape' to a sphere with a radius of 3 metres.
        shape = new SphereShape(3F);
        //Initialise 'Motion' to a motion state that assigns a specified location.
        motion = new DefaultMotionState(DEFAULT_BALL_TRANSFORM);
        //Calculate the inertia (resistance to movement) using the mass (2.5 kilograms).
        Vector3f ballInertia = new Vector3f(0, 0, 0);
        shape.calculateLocalInertia(mass, ballInertia);
        //Composes the construction info of its mass, its motion state, its shape, and its inertia.
        constructionInfo = new RigidBodyConstructionInfo(mass, motion, shape);
        //Set the restitution, also known as the bounciness or spring, to 0.5. The restitution may range from 0.0
        //not bouncy) to 1.0 (extremely bouncy).
        constructionInfo.restitution = 0.4f;
        //constructionInfo.angularDamping = 0.95f;
        //now we add it to our physics simulation 
        rig = new RigidBody(constructionInfo);
        // Disable 'sleeping' for the controllable ball.
        rig.setActivationState(CollisionObject.DISABLE_DEACTIVATION);
    }

    public float getMaxSpeed() {
        return maxSpeed;
    }
    
    
}
