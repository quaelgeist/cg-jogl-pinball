///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package de.hsos.cg.objects;
//
//import de.hsos.cg.objects.loader.*;
//import javax.media.opengl.GL2;
//
///**
// *
// * @author Jochen Gildner
// */
//public class SingleTarget {
//    private GLModel target = null;
//    private float xLocation = 0;
//    private float yLocation = 0;
//    private float zLocation = 0;
//    
//    public SingleTarget(GL2 gl) {
//        target = ModelLoaderOBJ.LoadModel("./resources/models/Ziel.obj", "./resources/models/Material.mtl", gl);
//    }
//    
//    public SingleTarget(GL2 gl, float xLocation, float yLocation, float zLocation) {
//        this.xLocation = xLocation;
//        this.yLocation = yLocation;
//        this.zLocation = zLocation;
//        target = ModelLoaderOBJ.LoadModel("./resources/models/Ziele.obj", "./resources/models/Material.mtl", gl);
//    }
//    
//    public float getxLocation() {
//        return xLocation;
//    }
//
//    public float getyLocation() {
//        return yLocation;
//    }
//
//    public float getzLocation() {
//        return zLocation;
//    }
//    
//    public void drawModel(GL2 gl){
//        target.opengldraw(gl);
//    }
//}
